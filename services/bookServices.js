const Book = require('../models/Book');
const Author = require('../models/Author');

module.exports.getBookList = async () => {
    const response = await Book.findAll();
    if(!response){
        throw new Error('No books');
    }
    const list = await response.map(book => book.toJSON());
    return list;
}

module.exports.getBookById = async (id) => {
    const response = await Book.findByPk(id);
    if(!response){
        throw new Error('Wrong book id');
    }
    const book = response.toJSON();
    return book;
}

module.exports.addBook = async (book) => {

    const { title, description, year, name, surname} = book;
    let authorId;

    const response = await Author.findOne({where: {name, surname}});

    if(!response){
        const createAuthor = await Author.create({name, surname});
        const newAuthor = await createAuthor.toJSON();
        id = newAuthor.id;
    } else {
        const formatedResponse = response.toJSON();
        id = formatedResponse.id;
    }

    const bookCreate = await Book.create({title, description, year, authorId});
    const newBook = await bookCreate.toJSON();

    return newBook;
}

module.exports.updateBook = async (book) => {

    const { id, title, description, year, name, surname} = book;

    if(title){
        await Book.update({title}, { where: {
            id
        }})
    }
    if(description){
        await Book.update({description}, { where: {
            id
        }})
    }
    if(year){
        await Book.update({year}, { where: {
            id
        }})
    }
    if(name){
        await Book.update({name}, { where: {
            id
        }})
    }
    if(surname){
        await Book.update({surname}, { where: {
            id
        }})
    }

    return id;
}

module.exports.deleteBook = async (id) => {
    const result = await Book.destroy({
        where: {
            id
        }
    })
    return result;
}