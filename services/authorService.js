const Author = require('../models/Author');

module.exports.getAuthorList = async () => {
    const response = await Author.findAll();
    if(!response){
        throw new Error('No Authors');
    }
    const list = await response.map(author => author.toJSON());
    return list;
}

module.exports.getAuthorById = async (id) => {
    const response = await Author.findByPk(id);
    if(!response){
        throw new Error('Wrong author id');
    }
    const author = response.toJSON();
    return author;
}

module.exports.addAuthor = async (author) => {

    const { name, surname, birth } = author;

    const aurhorCreate = await Author.create({ name, surname, birth });
    const newAuthor = await aurhorCreate.toJSON();

    return newAuthor;
}

module.exports.updateAuthor = async (author) => {

    const { id, name, surname, birth } = author;

    if(name){
        await Author.update({name}, { where: {
            id
        }})
    }
    if(surname){
        await Author.update({surname}, { where: {
            id
        }})
    }
    if(birth){
        await Author.update({birth}, { where: {
            id
        }})
    }

    return id;
}

module.exports.deleteAuthor = async (id) => {
    const result = await Author.destroy({
        where: {
            id
        }
    })
    return result;
}