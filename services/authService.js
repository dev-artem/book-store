const User = require('../models/User');

module.exports.signup = async (email, username, password) => {
    const result = await User.create({email, username, password});
    const user = result.toJSON();
    return user.id;
}

module.exports.login = async (email, password) => {
    const result = await User.findOne({
        where: {
            email
        }
    });
    const valid = await result.validPassword(password);
    if(!valid){
        throw new Error();
    }
    const user = await result.toJSON();
    return user.id;
}