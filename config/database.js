const { Sequelize } = require('sequelize');

module.exports = new Sequelize("book_store", "artem", "artemdb", {
    host: 'localhost',
    dialect: 'postgres',
    define: {
        timestamps: false
    }
});