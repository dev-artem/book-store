const { Router } = require('express');
const router = Router();
const controller = require('../controllers/bookController');

router.get('/books', controller.getBookList);
router.get('/books/:id', controller.getBookById);

router.post('/books/add', controller.addBook);

router.put('/books/:id/edit', controller.updateBook);

router.delete('/books/:id', controller.deleteBook);

module.exports = router;