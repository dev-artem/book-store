const { Router } = require('express');
const router = Router();
const controller = require('../controllers/authController');

router.post('/signup', controller.signup);
router.post('/login', controller.login);
router.get('/logout', controller.logout);

module.exports = router;