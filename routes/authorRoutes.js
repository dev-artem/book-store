const { Router } = require('express');
const router = Router();
const controller = require('../controllers/authorController');

router.get('/authors', controller.getAuthorList);
router.get('/authors/:id', controller.getAuthorById);

router.post('/authors/add', controller.addAuthor);

router.put('/authors/:id/edit', controller.updateAuthor);

router.delete('/authors/:id', controller.deleteAuthor);

module.exports = router;