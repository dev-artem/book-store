const { DataTypes } = require('sequelize');
const db = require('../config/database');

const Book = db.define('book', {
    title: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        defaultValue: 'No description'
    },
    year: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    authorId: {
        type: DataTypes.INTEGER
    }
})

module.exports = Book;