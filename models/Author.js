const { DataTypes } = require('sequelize');
const db = require('../config/database');
const Book = require('./Book');

const Author = db.define("author", {
    name: {
        type: DataTypes.STRING(20)
    },
    surname: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    birth: {
        type: DataTypes.INTEGER
    }
})
Author.hasMany(Book);

module.exports = Author;