const { DataTypes } = require('sequelize')
const db = require('../config/database');
const bcrypt = require('bcrypt');

const User = db.define("user",{
    email: {
        type: DataTypes.STRING(30),
        unique: true,
        allowNull: false
    },
    username: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    hooks: {
      beforeCreate: async (user) => {
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(user.password, salt);
      }
    }  
})

User.prototype.validPassword = async function(password) {
  return await bcrypt.compare(password, this.password);
}

module.exports = User;