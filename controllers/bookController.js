const service = require('../services/bookServices');

module.exports.getBookList = async (req, res) => {    
    try {
        const list = await service.getBookList();
        res.status(200).json(list);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.getBookById = async (req, res) => {  
    const id = Number(req.params.id);  
    try {
        const book = await service.getBookById(id);
        res.status(200).json(book);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.addBook = async (req, res) => {
    const {title, description, year, name, surname} = req.body;
    
    try {
        const book = await service.addBook({title, description, year, name, surname});
        res.status(201).json(book);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.updateBook = async (req, res) => {
    const { title, description, year, name, surname} = req.body;
    const id = req.params.id;

    try {
        const response = await service.updateBook({id: Number(id), title, description, year, name, surname});
        const book = await service.getBookById(response);
        res.status(201).json(book);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.deleteBook = async (req, res) => {
    const id = Number(req.params.id);  
    try {
        const result = await service.deleteBook(id);
        res.status(200).json({deleted: !!result});
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}