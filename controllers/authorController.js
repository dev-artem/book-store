const service = require('../services/authorService');

module.exports.getAuthorList = async (req, res) => {    
    try {
        const list = await service.getAuthorList();
        res.status(200).json(list);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.getAuthorById = async (req, res) => {  
    const id = Number(req.params.id);  
    try {
        const author = await service.getAuthorById(id);
        res.status(200).json(author);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.addAuthor = async (req, res) => {
    const { name, surname, birth } = req.body;
    
    try {
        const author = await service.addAuthor({ name, surname, birth });
        res.status(201).json(author);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.updateAuthor = async (req, res) => {
    const { name, surname, birth } = req.body;
    const id = req.params.id;

    try {
        const response = await service.updateAuthor({id: Number(id), name, surname, birth});
        const author = await service.getAuthorById(response);
        res.status(201).json(author);
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}

module.exports.deleteAuthor = async (req, res) => {
    const id = Number(req.params.id);  
    try {
        const result = await service.deleteAuthor(id);
        res.status(200).json({deleted: !!result});
    } catch (err) {
        console.log(err);
        res.status(400).json({err});
    }
}