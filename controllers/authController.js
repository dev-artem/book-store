const service = require('../services/authService');
const jwt = require('jsonwebtoken')

const maxAge = 24 * 60 * 60;
const createToken = (id) => {
    return jwt.sign({id}, 'token secret', {
        expiresIn: maxAge
    })
}

module.exports.signup = async (req, res) => {
    const { email, username, password } = req.body;
    try {
        const userId = await service.signup(email,username,password);
        const token = createToken(userId);
        res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge * 1000});
        res.status(201).json({user: userId});
    } catch (err) {
        console.log(err);
        res.status(400).json(err);
    }
}

module.exports.login = async (req, res) => {
    const { email, password } = req.body;
    try {
        const userId = await service.login(email, password);
        const token = createToken(userId);
        res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge * 1000});
        res.status(201).json({user: userId});
    } catch (err) {
        console.log(err);
        res.status(400).json(err);        
    }
}

module.exports.logout = async (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/');
}
