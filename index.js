const path = require('path');
const dotenv = require('dotenv');
const express = require('express');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');

const bookRoutes = require('./routes/bookRoutes');
const authorRoutes = require('./routes/authorRoutes');
const authRoutes = require('./routes/authRoutes');

const db = require('./config/database');

dotenv.config();

const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(cookieParser());
app.use(morgan('tiny'));

app.use('/api', authRoutes);
app.use('/api', bookRoutes);
app.use('/api', authorRoutes);

db.sync()
.then(() => {
    console.log('Synchronization')
})
.catch(err => {
    console.log(err)
})

const port = process.env.PORT || 3001;

app.listen(port, (err) => {
    if(err){
        return console.log(err);
    }
    console.log(`Server running on port: ${port}.`)
})
